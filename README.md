# ViSliCo
ViSliCo (Video Slides Connecotor) je program na spojovaní multimediálních dat jako jsou video a prezentace, obsahující funkcionalitu na prací s nimi. 

## Distribuce
V tomto repozitáři jsou umístěny dvě varianty mého programu ViSliCo: EXE a PYTHON. 

### EXE
Pro zprovoznění varianty EXE stačí stáhnout složku a spouštět aplikaci přes soubor VySliCO.exe,
na který je možné si vytvořit zástupce a umístit například na plochu.

Při používání programu mohou nastat potíže, které jsou způsobeny chybějícímy knihovnami ve vašem počítači.
Pro odstranění těchto potíží je aktuálně možné podívat se na chybovou hlášku při nečekaném ukončení programu a doinstalovat si potřebnou knihovnu.
Odkazy na stažení a případně potřebné informace můžete naleznout níže, v sekci "základní chybějící balíčky". 

### PYTHON 
Pro variantu v pythonu, která není brána jako hlavní verze distribuce je v repozitáři přiložen soubor requirements.txt.
Takže pokud by jste chtěli spustit program přes python, je potřeba zadat do terminálu 'pip install -r requirements.txt' pro stažení veškerých potřebních knihoven.

### Základní chybějící balíčky a jejich chyby
Základní chybějící baličky, které je potřeba stáhnout, nainstalovat a přidat do enviroment variables (systémových proměnných), pokud je ještě nemáte: 

FFMPEG - ODKAZ KE STAŽENÍ: https://ffmpeg.org/download.html
Pokud ho nemáte tak vám nepojede generovani titulku. 
Videotutorial na jeho instalaci na windows: https://www.youtube.com/watch?time_continue=503&v=22vmzTs5BoE&feature=emb_title
Po instalaci je potřeba vymazat složku cache.

POPPLER - ODKAZ KE STAŽENÍ: https://blog.alivate.com.au/poppler-windows/
Pokud ho nemáte tak po přidání pdf filu program, vám vyhodí chybu 'pdf2image.execpion'
Instaluje se podobně jako FFMPEG. Detajlnějši instrukce se dá najít tady https://stackoverflow.com/questions/18381713/how-to-install-poppler-on-windows
Po instalaci je potřeba vymazat složku cache.

KLite-Codec - ODKAZ KE STAŽENÍ: https://fileforum.com/download/KLite-Codec-Pack_Standard/1094057842/2
Vyberte si nejvíce základní nastavení. Pokud ho nemáte tak vám nepojede videoplayer.

Taky by jste se mohli potkat s chybovou hláškou na místě vygenerovaných titulek. V děné situaci je potřeba vejít do složky cache, najít složku se jménem daného videa a smazat soubory .src