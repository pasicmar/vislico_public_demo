# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'main_windowGRmLBR.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################


from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtMultimediaWidgets import QVideoWidget

import view.iconsqrt_rc


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if not MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(1290, 738)
        MainWindow.setStyleSheet(u"*{\n"
"border:none\n"
"}")
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        self.centralwidget.setStyleSheet(u"background-color:rgb(255,255,255)")
        self.horizontalLayout = QHBoxLayout(self.centralwidget)
        self.horizontalLayout.setSpacing(0)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.slide_menu = QFrame(self.centralwidget)
        self.slide_menu.setObjectName(u"slide_menu")
        sizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.slide_menu.sizePolicy().hasHeightForWidth())
        self.slide_menu.setSizePolicy(sizePolicy)
        self.slide_menu.setMinimumSize(QSize(0, 0))
        self.slide_menu.setMaximumSize(QSize(500, 16777215))
        self.slide_menu.setAutoFillBackground(False)
        self.slide_menu.setStyleSheet(u"background-color: rgb(255, 250, 245)")
        self.slide_menu.setFrameShape(QFrame.StyledPanel)
        self.slide_menu.setFrameShadow(QFrame.Raised)
        self.verticalLayout_4 = QVBoxLayout(self.slide_menu)
        self.verticalLayout_4.setSpacing(0)
        self.verticalLayout_4.setObjectName(u"verticalLayout_4")
        self.verticalLayout_4.setContentsMargins(4, 0, 0, 0)
        self.frame_2 = QFrame(self.slide_menu)
        self.frame_2.setObjectName(u"frame_2")
        self.frame_2.setFrameShape(QFrame.StyledPanel)
        self.frame_2.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_9 = QHBoxLayout(self.frame_2)
        self.horizontalLayout_9.setSpacing(10)
        self.horizontalLayout_9.setObjectName(u"horizontalLayout_9")
        self.horizontalLayout_9.setContentsMargins(7, 10, 0, 10)
        self.label_2 = QLabel(self.frame_2)
        self.label_2.setObjectName(u"label_2")
        font = QFont()
        font.setPointSize(12)
        font.setBold(True)
        font.setItalic(False)
        font.setUnderline(False)
        font.setWeight(75)
        font.setStrikeOut(False)
        font.setKerning(True)
        font.setStyleStrategy(QFont.PreferDefault)
        self.label_2.setFont(font)
        self.label_2.setMargin(0)

        self.horizontalLayout_9.addWidget(self.label_2, 0, Qt.AlignHCenter|Qt.AlignVCenter)

        self.label_3 = QLabel(self.frame_2)
        self.label_3.setObjectName(u"label_3")
        self.label_3.setMinimumSize(QSize(20, 20))
        self.label_3.setMaximumSize(QSize(30, 30))
        self.label_3.setPixmap(QPixmap(u":/icons/icons/gallery.png"))
        self.label_3.setScaledContents(True)
        self.label_3.setMargin(-1)

        self.horizontalLayout_9.addWidget(self.label_3, 0, Qt.AlignLeft)


        self.verticalLayout_4.addWidget(self.frame_2)

        self.start_pdf_frame = QFrame(self.slide_menu)
        self.start_pdf_frame.setObjectName(u"start_pdf_frame")
        sizePolicy1 = QSizePolicy(QSizePolicy.Maximum, QSizePolicy.Preferred)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.start_pdf_frame.sizePolicy().hasHeightForWidth())
        self.start_pdf_frame.setSizePolicy(sizePolicy1)
        self.start_pdf_frame.setMinimumSize(QSize(300, 0))
        self.start_pdf_frame.setMaximumSize(QSize(5000, 16777215))
        self.start_pdf_frame.setFrameShape(QFrame.StyledPanel)
        self.start_pdf_frame.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_4 = QHBoxLayout(self.start_pdf_frame)
        self.horizontalLayout_4.setSpacing(0)
        self.horizontalLayout_4.setObjectName(u"horizontalLayout_4")
        self.horizontalLayout_4.setContentsMargins(0, 0, 0, 0)
        self.start_presentation_frame = QFrame(self.start_pdf_frame)
        self.start_presentation_frame.setObjectName(u"start_presentation_frame")
        sizePolicy2 = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Preferred)
        sizePolicy2.setHorizontalStretch(0)
        sizePolicy2.setVerticalStretch(0)
        sizePolicy2.setHeightForWidth(self.start_presentation_frame.sizePolicy().hasHeightForWidth())
        self.start_presentation_frame.setSizePolicy(sizePolicy2)
        self.start_presentation_frame.setMinimumSize(QSize(0, 0))
        self.start_presentation_frame.setMaximumSize(QSize(500, 16777215))
        self.start_presentation_frame.setFrameShape(QFrame.StyledPanel)
        self.start_presentation_frame.setFrameShadow(QFrame.Raised)
        self.verticalLayout_3 = QVBoxLayout(self.start_presentation_frame)
        self.verticalLayout_3.setSpacing(0)
        self.verticalLayout_3.setObjectName(u"verticalLayout_3")
        self.verticalLayout_3.setContentsMargins(0, 0, 0, 0)
        self.select_presentation_button = QPushButton(self.start_presentation_frame)
        self.select_presentation_button.setObjectName(u"select_presentation_button")
        sizePolicy3 = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        sizePolicy3.setHorizontalStretch(0)
        sizePolicy3.setVerticalStretch(0)
        sizePolicy3.setHeightForWidth(self.select_presentation_button.sizePolicy().hasHeightForWidth())
        self.select_presentation_button.setSizePolicy(sizePolicy3)
        self.select_presentation_button.setMaximumSize(QSize(16777215, 16777215))
        font1 = QFont()
        font1.setPointSize(9)
        self.select_presentation_button.setFont(font1)
        icon = QIcon()
        icon.addFile(u":/icons/icons/pdf-file.png", QSize(), QIcon.Normal, QIcon.Off)
        self.select_presentation_button.setIcon(icon)

        self.verticalLayout_3.addWidget(self.select_presentation_button)


        self.horizontalLayout_4.addWidget(self.start_presentation_frame)

        self.tabWidget = QTabWidget(self.start_pdf_frame)
        self.tabWidget.setObjectName(u"tabWidget")
        self.tabWidget.setMaximumSize(QSize(0, 16777215))
        self.connections = QWidget()
        self.connections.setObjectName(u"connections")
        self.horizontalLayout_8 = QHBoxLayout(self.connections)
        self.horizontalLayout_8.setSpacing(0)
        self.horizontalLayout_8.setObjectName(u"horizontalLayout_8")
        self.horizontalLayout_8.setContentsMargins(0, 0, 0, 0)
        self.slides_scrollArea = QScrollArea(self.connections)
        self.slides_scrollArea.setObjectName(u"slides_scrollArea")
        self.slides_scrollArea.setMaximumSize(QSize(500, 16777215))
        self.slides_scrollArea.setWidgetResizable(True)
        self.scrollAreaWidgetContents = QWidget()
        self.scrollAreaWidgetContents.setObjectName(u"scrollAreaWidgetContents")
        self.scrollAreaWidgetContents.setGeometry(QRect(0, 0, 16, 570))
        self.slides_scrollArea.setWidget(self.scrollAreaWidgetContents)

        self.horizontalLayout_8.addWidget(self.slides_scrollArea)

        self.tabWidget.addTab(self.connections, "")
        self.word_search_result = QWidget()
        self.word_search_result.setObjectName(u"word_search_result")
        self.horizontalLayout_13 = QHBoxLayout(self.word_search_result)
        self.horizontalLayout_13.setObjectName(u"horizontalLayout_13")
        self.text_scrollArea = QScrollArea(self.word_search_result)
        self.text_scrollArea.setObjectName(u"text_scrollArea")
        self.text_scrollArea.setMaximumSize(QSize(500, 16777215))
        self.text_scrollArea.setWidgetResizable(True)
        self.scrollAreaWidgetContents_2 = QWidget()
        self.scrollAreaWidgetContents_2.setObjectName(u"scrollAreaWidgetContents_2")
        self.scrollAreaWidgetContents_2.setGeometry(QRect(0, 0, 16, 548))
        self.text_scrollArea.setWidget(self.scrollAreaWidgetContents_2)

        self.horizontalLayout_13.addWidget(self.text_scrollArea)

        self.tabWidget.addTab(self.word_search_result, "")

        self.horizontalLayout_4.addWidget(self.tabWidget)

        self.language_select_frame = QFrame(self.start_pdf_frame)
        self.language_select_frame.setObjectName(u"language_select_frame")
        sizePolicy1.setHeightForWidth(self.language_select_frame.sizePolicy().hasHeightForWidth())
        self.language_select_frame.setSizePolicy(sizePolicy1)
        self.language_select_frame.setMaximumSize(QSize(0, 16777215))
        font2 = QFont()
        font2.setPointSize(3)
        self.language_select_frame.setFont(font2)
        self.language_select_frame.setFrameShape(QFrame.StyledPanel)
        self.language_select_frame.setFrameShadow(QFrame.Raised)
        self.verticalLayout_5 = QVBoxLayout(self.language_select_frame)
        self.verticalLayout_5.setSpacing(0)
        self.verticalLayout_5.setObjectName(u"verticalLayout_5")
        self.verticalLayout_5.setContentsMargins(0, 200, 0, 200)
        self.pushButton = QPushButton(self.language_select_frame)
        self.pushButton.setObjectName(u"pushButton")
        sizePolicy4 = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)
        sizePolicy4.setHorizontalStretch(0)
        sizePolicy4.setVerticalStretch(0)
        sizePolicy4.setHeightForWidth(self.pushButton.sizePolicy().hasHeightForWidth())
        self.pushButton.setSizePolicy(sizePolicy4)
        self.pushButton.setFont(font1)

        self.verticalLayout_5.addWidget(self.pushButton, 0, Qt.AlignHCenter)

        self.pdf_language_box = QComboBox(self.language_select_frame)
        self.pdf_language_box.setObjectName(u"pdf_language_box")
        sizePolicy5 = QSizePolicy(QSizePolicy.Maximum, QSizePolicy.Expanding)
        sizePolicy5.setHorizontalStretch(0)
        sizePolicy5.setVerticalStretch(0)
        sizePolicy5.setHeightForWidth(self.pdf_language_box.sizePolicy().hasHeightForWidth())
        self.pdf_language_box.setSizePolicy(sizePolicy5)
        self.pdf_language_box.setMaximumSize(QSize(200, 16777215))

        self.verticalLayout_5.addWidget(self.pdf_language_box, 0, Qt.AlignHCenter|Qt.AlignVCenter)


        self.horizontalLayout_4.addWidget(self.language_select_frame)


        self.verticalLayout_4.addWidget(self.start_pdf_frame)

        self.frame_9 = QFrame(self.slide_menu)
        self.frame_9.setObjectName(u"frame_9")
        self.frame_9.setFrameShape(QFrame.StyledPanel)
        self.frame_9.setFrameShadow(QFrame.Raised)
        self.verticalLayout_6 = QVBoxLayout(self.frame_9)
        self.verticalLayout_6.setSpacing(17)
        self.verticalLayout_6.setObjectName(u"verticalLayout_6")
        self.verticalLayout_6.setContentsMargins(5, 0, 0, 14)
        self.change_video_btn = QPushButton(self.frame_9)
        self.change_video_btn.setObjectName(u"change_video_btn")
        self.change_video_btn.setFont(font1)
        icon1 = QIcon()
        icon1.addFile(u":/icons/icons/movie.png", QSize(), QIcon.Normal, QIcon.Off)
        self.change_video_btn.setIcon(icon1)

        self.verticalLayout_6.addWidget(self.change_video_btn)

        self.change_presentation_btn = QPushButton(self.frame_9)
        self.change_presentation_btn.setObjectName(u"change_presentation_btn")
        font3 = QFont()
        font3.setPointSize(9)
        font3.setBold(False)
        font3.setWeight(50)
        self.change_presentation_btn.setFont(font3)
        self.change_presentation_btn.setIcon(icon)
        self.change_presentation_btn.setIconSize(QSize(20, 20))

        self.verticalLayout_6.addWidget(self.change_presentation_btn)


        self.verticalLayout_4.addWidget(self.frame_9)


        self.horizontalLayout.addWidget(self.slide_menu)

        self.videoplayer = QFrame(self.centralwidget)
        self.videoplayer.setObjectName(u"videoplayer")
        self.videoplayer.setFrameShape(QFrame.StyledPanel)
        self.videoplayer.setFrameShadow(QFrame.Raised)
        self.verticalLayout = QVBoxLayout(self.videoplayer)
        self.verticalLayout.setSpacing(0)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.header_frame = QFrame(self.videoplayer)
        self.header_frame.setObjectName(u"header_frame")
        self.header_frame.setStyleSheet(u"background-color: rgb(255, 250, 240)")
        self.header_frame.setFrameShape(QFrame.StyledPanel)
        self.header_frame.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_3 = QHBoxLayout(self.header_frame)
        self.horizontalLayout_3.setSpacing(0)
        self.horizontalLayout_3.setObjectName(u"horizontalLayout_3")
        self.horizontalLayout_3.setContentsMargins(0, 5, 0, 0)
        self.frame_7 = QFrame(self.header_frame)
        self.frame_7.setObjectName(u"frame_7")
        self.frame_7.setFrameShape(QFrame.StyledPanel)
        self.frame_7.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_7 = QHBoxLayout(self.frame_7)
        self.horizontalLayout_7.setSpacing(0)
        self.horizontalLayout_7.setObjectName(u"horizontalLayout_7")
        self.horizontalLayout_7.setContentsMargins(0, 0, 0, 0)
        self.pushButton_6 = QPushButton(self.frame_7)
        self.pushButton_6.setObjectName(u"pushButton_6")
        icon2 = QIcon()
        icon2.addFile(u":/icons/icons/chevron.png", QSize(), QIcon.Normal, QIcon.Off)
        self.pushButton_6.setIcon(icon2)
        self.pushButton_6.setIconSize(QSize(30, 30))

        self.horizontalLayout_7.addWidget(self.pushButton_6, 0, Qt.AlignLeft|Qt.AlignTop)


        self.horizontalLayout_3.addWidget(self.frame_7, 0, Qt.AlignLeft|Qt.AlignVCenter)

        self.frame = QFrame(self.header_frame)
        self.frame.setObjectName(u"frame")
        self.frame.setEnabled(True)
        sizePolicy4.setHeightForWidth(self.frame.sizePolicy().hasHeightForWidth())
        self.frame.setSizePolicy(sizePolicy4)
        self.frame.setMinimumSize(QSize(0, 0))
        self.frame.setSizeIncrement(QSize(0, 0))
        self.frame.setBaseSize(QSize(0, 0))
        self.frame.setLayoutDirection(Qt.LeftToRight)
        self.frame.setAutoFillBackground(False)
        self.frame.setFrameShape(QFrame.StyledPanel)
        self.frame.setFrameShadow(QFrame.Raised)
        self.frame.setLineWidth(1)
        self.frame.setMidLineWidth(0)
        self.horizontalLayout_6 = QHBoxLayout(self.frame)
        self.horizontalLayout_6.setSpacing(0)
        self.horizontalLayout_6.setObjectName(u"horizontalLayout_6")
        self.horizontalLayout_6.setContentsMargins(0, 0, 80, 0)
        self.lineEdit = QLineEdit(self.frame)
        self.lineEdit.setObjectName(u"lineEdit")
        self.lineEdit.setMinimumSize(QSize(240, 30))
        self.lineEdit.setFont(font1)
        self.lineEdit.setStyleSheet(u"\n"
"border-bottom: 3 solid rgb(48,213,200)")

        self.horizontalLayout_6.addWidget(self.lineEdit, 0, Qt.AlignLeft|Qt.AlignTop)

        self.search_btn = QPushButton(self.frame)
        self.search_btn.setObjectName(u"search_btn")
        icon3 = QIcon()
        icon3.addFile(u":/icons/icons/loupe.png", QSize(), QIcon.Normal, QIcon.Off)
        self.search_btn.setIcon(icon3)
        self.search_btn.setIconSize(QSize(25, 25))

        self.horizontalLayout_6.addWidget(self.search_btn, 0, Qt.AlignLeft|Qt.AlignTop)


        self.horizontalLayout_3.addWidget(self.frame, 0, Qt.AlignLeft|Qt.AlignTop)

        self.generate_document_button = QPushButton(self.header_frame)
        self.generate_document_button.setObjectName(u"generate_document_button")
        font4 = QFont()
        font4.setPointSize(8)
        self.generate_document_button.setFont(font4)
        icon4 = QIcon()
        icon4.addFile(u":/icons/icons/write.png", QSize(), QIcon.Normal, QIcon.Off)
        self.generate_document_button.setIcon(icon4)
        self.generate_document_button.setIconSize(QSize(25, 30))

        self.horizontalLayout_3.addWidget(self.generate_document_button)

        self.frame_3 = QFrame(self.header_frame)
        self.frame_3.setObjectName(u"frame_3")
        self.frame_3.setFrameShape(QFrame.StyledPanel)
        self.frame_3.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_5 = QHBoxLayout(self.frame_3)
        self.horizontalLayout_5.setSpacing(10)
        self.horizontalLayout_5.setObjectName(u"horizontalLayout_5")
        self.horizontalLayout_5.setContentsMargins(0, 5, 5, 0)
        self.restore_or_maximize = QPushButton(self.frame_3)
        self.restore_or_maximize.setObjectName(u"restore_or_maximize")
        icon5 = QIcon()
        icon5.addFile(u":/icons/icons/diagonal-arrow.png", QSize(), QIcon.Normal, QIcon.Off)
        self.restore_or_maximize.setIcon(icon5)

        self.horizontalLayout_5.addWidget(self.restore_or_maximize)

        self.close = QPushButton(self.frame_3)
        self.close.setObjectName(u"close")
        icon6 = QIcon()
        icon6.addFile(u":/icons/icons/close.png", QSize(), QIcon.Normal, QIcon.Off)
        self.close.setIcon(icon6)

        self.horizontalLayout_5.addWidget(self.close)


        self.horizontalLayout_3.addWidget(self.frame_3, 0, Qt.AlignRight|Qt.AlignTop)


        self.verticalLayout.addWidget(self.header_frame, 0, Qt.AlignTop)

        self.widget = QVideoWidget(self.videoplayer)
        self.widget.setObjectName(u"widget")
        sizePolicy4.setHeightForWidth(self.widget.sizePolicy().hasHeightForWidth())
        self.widget.setSizePolicy(sizePolicy4)
        self.widget.setMaximumSize(QSize(16777215, 16777215))
        self.horizontalLayout_11 = QHBoxLayout(self.widget)
        self.horizontalLayout_11.setSpacing(0)
        self.horizontalLayout_11.setObjectName(u"horizontalLayout_11")
        self.horizontalLayout_11.setContentsMargins(0, 0, 0, 0)
        self.start_frame = QFrame(self.widget)
        self.start_frame.setObjectName(u"start_frame")
        self.start_frame.setFrameShape(QFrame.StyledPanel)
        self.start_frame.setFrameShadow(QFrame.Raised)
        self.verticalLayout_2 = QVBoxLayout(self.start_frame)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.verticalLayout_2.setContentsMargins(-1, 80, -1, -1)
        self.select_video_button = QPushButton(self.start_frame)
        self.select_video_button.setObjectName(u"select_video_button")
        sizePolicy3.setHeightForWidth(self.select_video_button.sizePolicy().hasHeightForWidth())
        self.select_video_button.setSizePolicy(sizePolicy3)
        self.select_video_button.setFont(font1)
        self.select_video_button.setIcon(icon1)

        self.verticalLayout_2.addWidget(self.select_video_button)

        self.label_5 = QLabel(self.start_frame)
        self.label_5.setObjectName(u"label_5")

        self.verticalLayout_2.addWidget(self.label_5, 0, Qt.AlignHCenter|Qt.AlignTop)


        self.horizontalLayout_11.addWidget(self.start_frame)


        self.verticalLayout.addWidget(self.widget)

        self.vieoplayer_frame = QFrame(self.videoplayer)
        self.vieoplayer_frame.setObjectName(u"vieoplayer_frame")
        self.vieoplayer_frame.setMaximumSize(QSize(0, 0))
        self.vieoplayer_frame.setFrameShape(QFrame.StyledPanel)
        self.vieoplayer_frame.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_12 = QHBoxLayout(self.vieoplayer_frame)
        self.horizontalLayout_12.setSpacing(0)
        self.horizontalLayout_12.setObjectName(u"horizontalLayout_12")
        self.horizontalLayout_12.setContentsMargins(5, 2, 2, 2)
        self.titles_label = QLabel(self.vieoplayer_frame)
        self.titles_label.setObjectName(u"titles_label")
        self.titles_label.setFont(font1)

        self.horizontalLayout_12.addWidget(self.titles_label, 0, Qt.AlignHCenter|Qt.AlignBottom)


        self.verticalLayout.addWidget(self.vieoplayer_frame)

        self.player_controller_frame = QFrame(self.videoplayer)
        self.player_controller_frame.setObjectName(u"player_controller_frame")
        self.player_controller_frame.setFrameShape(QFrame.StyledPanel)
        self.player_controller_frame.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_10 = QHBoxLayout(self.player_controller_frame)
        self.horizontalLayout_10.setObjectName(u"horizontalLayout_10")
        self.volumeButton = QPushButton(self.player_controller_frame)
        self.volumeButton.setObjectName(u"volumeButton")
        icon7 = QIcon()
        icon7.addFile(u":/icons/icons/volume.png", QSize(), QIcon.Normal, QIcon.Off)
        self.volumeButton.setIcon(icon7)

        self.horizontalLayout_10.addWidget(self.volumeButton)

        self.volumeSlider = QSlider(self.player_controller_frame)
        self.volumeSlider.setObjectName(u"volumeSlider")
        self.volumeSlider.setMaximumSize(QSize(60, 15))
        self.volumeSlider.setValue(50)
        self.volumeSlider.setOrientation(Qt.Horizontal)

        self.horizontalLayout_10.addWidget(self.volumeSlider, 0, Qt.AlignLeft)

        self.play_btn = QPushButton(self.player_controller_frame)
        self.play_btn.setObjectName(u"play_btn")
        icon8 = QIcon()
        icon8.addFile(u":/icons/icons/play-button.png", QSize(), QIcon.Normal, QIcon.Off)
        self.play_btn.setIcon(icon8)
        self.play_btn.setIconSize(QSize(25, 25))

        self.horizontalLayout_10.addWidget(self.play_btn)

        self.timeSlider = QSlider(self.player_controller_frame)
        self.timeSlider.setObjectName(u"timeSlider")
        self.timeSlider.setOrientation(Qt.Horizontal)

        self.horizontalLayout_10.addWidget(self.timeSlider)

        self.timeNow = QLabel(self.player_controller_frame)
        self.timeNow.setObjectName(u"timeNow")

        self.horizontalLayout_10.addWidget(self.timeNow)

        self.timeAll = QLabel(self.player_controller_frame)
        self.timeAll.setObjectName(u"timeAll")

        self.horizontalLayout_10.addWidget(self.timeAll)

        self.speed_down_btn = QPushButton(self.player_controller_frame)
        self.speed_down_btn.setObjectName(u"speed_down_btn")
        font5 = QFont()
        font5.setPointSize(12)
        self.speed_down_btn.setFont(font5)

        self.horizontalLayout_10.addWidget(self.speed_down_btn)

        self.speed = QLabel(self.player_controller_frame)
        self.speed.setObjectName(u"speed")
        self.speed.setFont(font4)

        self.horizontalLayout_10.addWidget(self.speed)

        self.speed_up_btn = QPushButton(self.player_controller_frame)
        self.speed_up_btn.setObjectName(u"speed_up_btn")
        self.speed_up_btn.setFont(font5)

        self.horizontalLayout_10.addWidget(self.speed_up_btn)

        self.on_subtitless_button = QPushButton(self.player_controller_frame)
        self.on_subtitless_button.setObjectName(u"on_subtitless_button")
        icon9 = QIcon()
        icon9.addFile(u":/icons/icons/subtitle.png", QSize(), QIcon.Normal, QIcon.Off)
        self.on_subtitless_button.setIcon(icon9)
        self.on_subtitless_button.setIconSize(QSize(22, 22))

        self.horizontalLayout_10.addWidget(self.on_subtitless_button)

        self.subtitles_language_button = QPushButton(self.player_controller_frame)
        self.subtitles_language_button.setObjectName(u"subtitles_language_button")
        icon10 = QIcon()
        icon10.addFile(u":/icons/icons/subtitles.png", QSize(), QIcon.Normal, QIcon.Off)
        self.subtitles_language_button.setIcon(icon10)

        self.horizontalLayout_10.addWidget(self.subtitles_language_button)

        self.srt_languages_Box = QComboBox(self.player_controller_frame)
        self.srt_languages_Box.setObjectName(u"srt_languages_Box")
        sizePolicy2.setHeightForWidth(self.srt_languages_Box.sizePolicy().hasHeightForWidth())
        self.srt_languages_Box.setSizePolicy(sizePolicy2)
        self.srt_languages_Box.setMaximumSize(QSize(0, 16777215))
        self.srt_languages_Box.setFont(font4)
        self.srt_languages_Box.setMouseTracking(False)
        self.srt_languages_Box.setEditable(True)
        self.srt_languages_Box.setIconSize(QSize(50, 10))

        self.horizontalLayout_10.addWidget(self.srt_languages_Box)


        self.verticalLayout.addWidget(self.player_controller_frame, 0, Qt.AlignBottom)

        self.widget.raise_()
        self.header_frame.raise_()
        self.player_controller_frame.raise_()
        self.vieoplayer_frame.raise_()

        self.horizontalLayout.addWidget(self.videoplayer)

        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)

        self.tabWidget.setCurrentIndex(0)
        self.srt_languages_Box.setCurrentIndex(-1)


        QMetaObject.connectSlotsByName(MainWindow)
    # setupUi

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"MainWindow", None))
        self.label_2.setText(QCoreApplication.translate("MainWindow", u"ViSliCo", None))
        self.label_3.setText("")
        self.select_presentation_button.setText(QCoreApplication.translate("MainWindow", u"Select presentation (in pdf)", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.connections), QCoreApplication.translate("MainWindow", u"slide search", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.word_search_result), QCoreApplication.translate("MainWindow", u"text search", None))
        self.pushButton.setText(QCoreApplication.translate("MainWindow", u"  Select presentations language  ", None))
        self.change_video_btn.setText(QCoreApplication.translate("MainWindow", u"Change video", None))
        self.change_presentation_btn.setText(QCoreApplication.translate("MainWindow", u"Change pressentation", None))
        self.pushButton_6.setText("")
        self.lineEdit.setPlaceholderText(QCoreApplication.translate("MainWindow", u"Search", None))
        self.search_btn.setText("")
        self.generate_document_button.setText("")
        self.restore_or_maximize.setText("")
        self.close.setText("")
        self.select_video_button.setText(QCoreApplication.translate("MainWindow", u"Select video", None))
        self.label_5.setText(QCoreApplication.translate("MainWindow", u"*make sure that different files have different names", None))
        self.titles_label.setText("")
        self.volumeButton.setText("")
        self.play_btn.setText("")
        self.timeNow.setText("")
        self.timeAll.setText("")
        self.speed_down_btn.setText(QCoreApplication.translate("MainWindow", u"-", None))
        self.speed.setText(QCoreApplication.translate("MainWindow", u"1.0", None))
        self.speed_up_btn.setText(QCoreApplication.translate("MainWindow", u"+", None))
        self.on_subtitless_button.setText("")
        self.subtitles_language_button.setText("")
        self.srt_languages_Box.setCurrentText("")
    # retranslateUi

