
from model.support import *
from model.subtitles.srt_loader import *
from model.subtitles.translater_languages import *
from model.logger import *


class Controller():
    def __init__(self, video_path):
        self.support = Support()
        self.subtitle_path = None
        self.video_path = video_path  # THIS IS THE IDIFICATOR

    def get_subtitles(self, language):
        logging.debug('get subtitles for ' + language)
        subtitles_path = self.support.create_subtitles(language)
        logging.info('controller return path ')
        self.subtitle_path = subtitles_path
        self.srt_former = SrtFormer(self.subtitle_path)

        if (self.support.autosub == None or self.support.autosub.subtitlesReady()):
            self.srt_former.loadSublitles()

    def subtitle_exist(self, language):  # FIX ACCES NAMING
        return self.support.cache_supervisor.chech_existance(language) != None

    def get_source_and_path(self, language):  # FIX ACCES NAMING
        language_path = self.support.cache_supervisor.chech_existance(language)
        if language_path == None:
            language_path = self.support.cache_supervisor.create_srt_path(
                language)
        default_path = self.support.cache_supervisor.default_srt_path
        return language_path, default_path

    # FIX ACCES NAMING
    def create_and_get_subtitles_path(self, language_path, default_path, language):
        try:
            subtitles_path = self.support.check_create_subtitles(
                language_path, default_path, language)
        except:
            return None
        return subtitles_path

    def load_subtitles_from_path(self, subtitle_path):
        self.subtitle_path = subtitle_path
        self.srt_former = SrtFormer(self.subtitle_path)
        if (self.support.autosub == None or self.support.autosub.subtitlesReady()):
            self.srt_former.loadSublitles()

    def get_current_subtitles(self, miliseconds):
        if (not self.srt_former.isLoaded):
            logging.info('current_subtitles are not loaded')
            if (self.support.autosub == None or self.support.autosub.subtitlesReady()):
                self.srt_former.loadSublitles()
            else:
                return ""
        return self.srt_former.get_current_text(miliseconds)

    def get_slides(self, pdf_path):
        pass

    def get_slides_from_cache(self):
        logging.debug('controller get slides from cache')
        return self.support.slides

    def set_video(self):
        self.support.convert_video(self.video_path)

    def get_srt_avaluable_languages(self):
        return LANGUAGES

    def process_pdf(self, pdf_path):
        self.pdf = pdf_path
        logging.info('process_pdf ' + self.pdf)
        self.slides = self.support.convert_pdf(self.pdf)
        return self.slides

    def generic_subtitles_exist(self):
        return self.support.check_if_generic_exist()

    def get_connections_from_cache(self):
        logging.debug('get connections')
        result, is_finnal = self.support.connect()
        self.connections = result
        return is_finnal
    
    def get_connections_file_from_cache(self):
        return self.support.get_slides_from_cache()

    def search_text_usage(self, text,current_language):
        if(current_language!= None and not self.subtitle_exist(current_language)):
            return self.support.search_text(text,None),True #FIX ADD MESSAGE 
        else: 
            return self.support.search_text(text,current_language),False

    def generate_document(self,current_language):
        if(current_language!= None and not self.subtitle_exist(current_language)):
            return self.support.create_document(None),True 
        else: 
            return self.support.create_document(current_language),False
