from pdf2image import convert_from_path


class PdfConvertor():
  def __init__(self, pdf_adress, output_path) -> None:
    self.pdf_adress = pdf_adress
    self.output_path=output_path

  def convert(self):
    pass


class PdfToImageConvertor(PdfConvertor):
  
  def convert(self):
    images = convert_from_path(self.pdf_adress)
    for i in range(len(images)):
      # Save pages as images in the pdf
      images[i].save(self.output_path + '/slide' + str(i) + '.jpg', 'JPEG')


    