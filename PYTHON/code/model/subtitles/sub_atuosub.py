import os
import moviepy.editor
from moviepy import *
from view.select_window import *
from model.logger import *
from model.subtitles.supported_languages import supported_languages
import autosub
import shutil



class SubtitlesGenerator:
    def __init__(self, video_path, output_file) -> None:
        self.video_path = video_path
        self.output = output_file
        self.thread_is_done = False
        
    def generate_subtitles():
        pass
    
    # WAIT FOR THE SUBTITILES TO BE GENERATED
    def waitSubtitles():
        pass 

class Autosub(SubtitlesGenerator):
    
    def autosub_generate(self,video_path,output,language_code,tmp_file='audio.wav'):
        video = moviepy.editor.VideoFileClip(video_path)
        audio = video.audio
        # Replace the parameter with the location along with filename
        audio.write_audiofile(tmp_file)
        try:
            autosub.generate_subtitles(tmp_file,output=output,src_language=language_code,dst_language=language_code,subtitle_file_format='srt')
            logging.info("autosub generating went GOOD")
            #COPY FILE
        except Exception as e :
            logging.error("autosub generating FAILED")
            with open(output,'w') as selected:
                selected.write("""
1
00:00:00,000 --> 99:00:00,000
---- GENERATING OF SUBTITLES FAILED ----"""+str(e))
            
            
        try :
            target_lang_file = output.replace("generic.srt", str(language_code)+".srt")
            shutil.copyfile(output,target_lang_file)
            logging.info("Subtitles stored for targed language.")
        except:
            logging.info("Storing subtitles as target language failed.")
            
        os.remove(tmp_file)
        
        self.thread_is_done = True
    
    def subtitlesReady(self):
        return self.thread_is_done
        #return self.srt.ready()
        
    def waitSubtitles(self):
        self.pool.join()
        
    def get_video_avaluable_languages(self):
        return supported_languages

    def generate_subtitles(self):
        codes = self.get_video_avaluable_languages()
        language_code = select_window.select_from_list("Language of the video:",codes)
        logging.info('generate_subtitles'+language_code)
        from threading import Thread
        print("done")
        self.thread_is_done = False
        self.srt = Thread(target = Autosub.autosub_generate, args =(self,self.video_path,self.output,language_code,'audio.wav',))
        self.srt.start()


        
         
        
# def resource_path(relative_path):
#     """ Get absolute path to resource, works for dev and for PyInstaller """
#     try:
#         # PyInstaller creates a temp folder and stores path in _MEIPASS
#         base_path = sys._MEIPASS
#     except Exception:
#         base_path = os.path.abspath(".")

#     return os.path.join(base_path, relative_path)

