import os
from model.frames.frames_generator import CV2VideoFramesGenerator
from model.connector.slide_video_pairing import VideoSlideConnector
from model.document_creator.docs_creator import DocumentCreator
from model.slides.pdf_converotr import PdfToImageConvertor
from model.subtitles.sub_atuosub import Autosub
from model.subtitles.srt_loader import *
from model.cache_supervisor import CacheSupervisor
from model.logger import *
import glob
import json
import time 


class Support():
    def __init__(self):
        self.cache_supervisor = CacheSupervisor()
        self.autosub = None
        self.allConnections = None
        self.framesConnector = None
        self.srt_loader=None
        self.inverted = None
        self.searchers ={}

    def get_slides_from_cache(self):
        path = self.cache_supervisor.slides_folder
        if (path == None):
            logging.info(' none slides from cache')
            self.slides = None
        else:
            logging.info(path + 'slides from cache folder exist')
            self.slides = list(glob.iglob(f'{path}/*'))
            
    # creates slides from pdf
    def convert_pdf(self, pdf_path):
        logging.debug('to start lets convert pdf'+ pdf_path)
        if not self.cache_supervisor.add_presentation(pdf_path):
            self.slides_directory = self.cache_supervisor.slides_folder
            self.slides_generator = PdfToImageConvertor(
                pdf_path, self.slides_directory)
            self.slides_generator.convert()
        self.slides = list(glob.iglob(f'{self.cache_supervisor.slides_folder}/*'))
        return self.slides

    # creates frames from video
    def convert_video(self, video_path, seconds=30):
        logging.debug('conver video')
        if not (self.cache_supervisor.add_video(video_path, seconds)):
            logging.info('new video')
            self.framesGererator = CV2VideoFramesGenerator(
                video_path, self.cache_supervisor.frames_folder, seconds)
            self.framesGererator.videoConvertor()
            self.slides = None
        else:
            logging.info('old video')
            self.get_slides_from_cache()
        if not (self.check_if_generic_exist()):
            self.create_default_subtitles(video_path)
        self.frames = list(glob.iglob(
            f'{self.cache_supervisor.frames_folder}/*'))
        
    # default subtitles are done from automaticaly detected language
    def create_default_subtitles(self, video_path):
        print("DISABLE")
        self.default_path = self.cache_supervisor.default_srt_path
        logging.info('default subtitles are creating')
        self.autosub = Autosub(video_path, self.default_path)
        self.autosub.generate_subtitles() 

    # translate default subtitles to input language
    def create_subtitles(self, language):
        path = self.cache_supervisor.chech_existance(language)
        logging.debug('create_subtitles'+str(path))
        if path == None:
            logging.info('path == None')
            path = self.cache_supervisor.create_srt_path(language)
            self.srt_loader = SrtFormer(self.cache_supervisor.default_srt_path)
            # FIX aktivni cekani/ja potreba pockat az bude hotove generic
            self.srt_loader.loadSublitles()
            self.srt_loader.translate_srt(language, path)
        logging.info('new srt path ')
        return path
    
    def check_create_subtitles(self,language_path,default_path,language):
        logging.info('subtitles path == None')
        self.srt_loader = SrtFormer(default_path)
        while(not self.srt_loader.subtitle_file_exist()): #WAIT FOR GENERIC
            time.sleep(1)
        self.srt_loader.loadSublitles()
        self.srt_loader.translate_srt(language, language_path)
        return language_path

    def connect(self):
        logging.debug('lets connect it')
        connections_path = self.cache_supervisor.conections_file_path
        logging.debug('connections_path'+ str(connections_path))
        if not self.check_if_connection_exist():
            if (self.cache_supervisor.check_if_slides_folder_not_empty()):
                # IF PRESENTATION IS CHANGED - FRAME CONNECTOR HAS TO BE None
                if self.framesConnector == None:
                    self.framesConnector = VideoSlideConnector(
                        self.frames, self.slides, connections_path)
                    self.framesConnector.connect()
                if (not self.framesConnector.isReady()):
                    return None, False
                self.allConnections = self.framesConnector.storeData()
        else:
            with open("%s" % connections_path, 'r') as json_file:
                self.allConnections = json.load(json_file)
        return self.clear_timestamps(), True

    def create_document(self,language = None):
        self.clear_timestamps()
        if (self.inverted == None):
            conection = []
        else:
            conection = self.inverted
        
        if language == None:
            srt_file =self.cache_supervisor.default_srt_path
            language = "AUTODETECTED"
        else:
            srt_file = self.create_subtitles(language)
                
        doc_file = self.cache_supervisor.create_document_path(language)
        if doc_file == None:
            return None
        self.documentCreator = DocumentCreator(
            conection, self.slides, self.cache_supervisor.video_file_name, self.cache_supervisor.pdf_file_name, srt_file, doc_file)
        self.documentCreator.create_docs_file()
        return doc_file
        
    def search_text(self,text,language):
        if (language not in self.searchers):
            if(language == None):
                path = self.cache_supervisor.default_srt_path
            else:    
                path = self.create_subtitles(language)
                #FIX CHECK
            search_loader=SrtFormer(path)
            search_loader.loadSublitles()
            self.searchers[language]=search_loader
        else: 
            search_loader = self.searchers[language]
        return search_loader.get_time_of_usage(text)

    def check_if_generic_exist(self):
        return self.cache_supervisor.chech_existance('generic') != None

    def check_if_connection_exist(self):
        return self.cache_supervisor.chech_connections_existance()

    #changes connections format
    def clear_timestamps(self):
        # invert and sort
        if(not self.allConnections):
            return []
        inverted = {}
        for slide, times in self.allConnections.items():
            for tm in times:
                time = tuple(map(int, tm.split('.')[0].split('_')))
                inverted[time] = slide
        sin = sorted(inverted.items())
        self.inverted=sin
        # combine all frames into one slide, even if they have spaces
        result = {}
        if len(sin) == 0:
            return {}
        c_time, c_slide = sin[0]
        result[c_slide] = []
        result[c_slide].append(c_time)
        for time, slide in sin:
            if (slide != c_slide):
                c_slide = slide
                c_time = time
                if (c_slide not in result):
                    result[c_slide] = []
                result[c_slide].append(c_time)
        return result
    
    


